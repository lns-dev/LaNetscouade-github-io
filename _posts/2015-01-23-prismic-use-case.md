<p>Dans le cadre de la refonte de notre site, nous avons choisi d'utiliser Prismic pour la gestion du contenu.</p>
<p>Pourquoi avons-nous utilisé l'outil, comment, quels en sont les avantages et les inconvénients. Let's find it out !</p>
<h2>Contexte</h2>
<p>Notre ancien site - un Drupal 7 de 2010 - devait être refondu. Nous avons décidé de nous plonger dans Prismic plus en détail pour découvrir ses forces et ses faiblesses.</p>
<p>Découvrez dans cet article le détail de notre refonte.</p>
<h2>Avantages</h2>
<p>Le BackOffice est simple d'utilisation, l'API est disponible et répond rapidement. Multiplier les appels à l'API ne fait pas fléchir Prismic.</p>
<h2>Tips</h2>
<ul>
<li>
L'utilisation d'un "form" custom (à créer dans le Back Office de Prismic) peut largement réduire le nombre de vos requêtes, ou de les simplifier significativement si vous tentez de récupérer des documents de différents types :
<pre>
<code>var _getPrismicContent = function () {
  Prismic.ctx().then(function(ctx){
      ctx.api.form('community').ref(ctx.ref).query()
      .submit(function(error, docs) {
          // Do Stuff with your data
      });
  });
};</code>
</pre>
</li>
</ul>
<h2>Bonus</h2>
<p>L'équipe de Prismic est disponible et à l'écoute des problématiques utilisateur. Nous l'avons découvert qu'en fin de projet.</p>
<p>Il nous aurait été possible de réaliser cette reprise de contenus avec un support direct des développeurs de Prismic (création de masks, mapping, import).</p>
<p>Proposition nous a aussi été faite de passer sur leur plateforme expérimentale, qui offre des fonctionnalités en beta-testing : import de contenu, unique-slug, API améliorée.</p>
<p>N'hésitez donc pas à entrer en contact avec eux au moindre blocage.</p>
<p><strong>Bémols</strong></p>
<ul>
<li>Nativement, Prismic ne nous permet pas encore de fixer des slugs, et du coup d'interroger l'API à partir de ceux-ci. On passe donc par les Document.id pour récupérer les données, ce qui n'est pas de plus bel effet dans les URLs. (exemple : <a href="http://journal.lanetscouade.com/article/VK8ICCkAANsGvLlH/homos-la-haine--quelques-chiffres-sur-la">http://journal.lanetscouade.com/article/VK8ICCkAANsGvLlH/homos-la-haine--quelques-chiffres-sur-la</a>)</li>
<li>Vous notez également la mauvaise génération des slugs côté Prismic, qui mange le dernier terme du titre de notre article :'(</li>
</ul>