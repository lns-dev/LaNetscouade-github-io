<h1>Changelog</h1>
<h2>1.0.6 - 2014-03-10</h2>
<ul>
<li>Fixes append_elements bug <a href="https://github.com/rnmp/salvattore/issues/59">#59</a>. (from pull request <a href="https://github.com/rnmp/salvattore/pull/62">#62</a> by <a href="https://github.com/jlawrence-yellostudio">@jlawrence-yellostudio</a>)</li>
</ul>
<h2>1.0.5 - 2014-02-26</h2>
<ul>
<li>Fixes Chrome Canary bug (<a href="https://github.com/rnmp/salvattore/issues/40">#40</a> and <a href="https://github.com/rnmp/salvattore/issues/38">#38</a>)</li>
</ul>
<h2>1.0.4 - 2013-11-20</h2>
<ul>
<li>Fixes error regarding JS API</li>
</ul>
<h2>1.0.3 - 2013-11-20</h2>
<ul>
<li>Fixes <code>register_grid is not defined</code> error</li>
</ul>
<h2>1.0.2 - 2013-11-17</h2>
<ul>
<li>Source code rewritten in JS (previously CoffeeScript) for easier contribution!</li>
</ul>
<h2>1.0.1 - 2013-09-06</h2>
<ul>
<li>Fix column rearrangement on window resize. (<a href="https://github.com/rnmp/salvattore/issues/9">#9</a>)</li>
</ul>
<h2>1.0.0 - 2013-07-25</h2>
<ul>
<li>First release.</li>
</ul>