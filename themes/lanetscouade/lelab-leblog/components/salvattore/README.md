<h1>Salvattore</h1>
<p><a href="http://salvattore.com/"><img src="http://files.bandd.co/zQf5+" alt="" /></a></p>
<p>Salvattore is a library agnostic JS script that will help you organize your HTML elements according to the number of columns you specify, like jQuery Masonry.</p>
<h2>Features</h2>
<ul>
<li><strong>No requirements:</strong> Salvattore is a standalone script, it will work right away after being referenced in your HTML page.</li>
<li><strong>Extremely lightweight:</strong> about 2.5KB (minified and gzipped.)</li>
<li><strong>CSS-driven configuration:</strong> the number of columns is defined in CSS and the styling is left to the user.</li>
<li><strong>Media queries ready:</strong> the same parameters can be used inside media queries for better results on different devices.</li>
<li><strong>Wide browser support:</strong> modern browsers and IE9+ (though we're working on IE8.)</li>
</ul>
<h3>Upcoming</h3>
<ul>
<li><strong>IE8 support:</strong> without media queries, they aren't needed anyway.</li>
<li><strong>Balanced columns:</strong> to keep all columns about the same height.</li>
</ul>
<p>To find out more and see it in action, please visit <a href="http://salvattore.com">our website.</a></p>
<p>You can also <a href="http://twitter.com/salvattorejs">follow us</a> on Twitter.</p>
<h2>How to contribute</h2>
<p>We use Grunt to add polyfills and minify the script in the <code>dist/</code> folder. To make changes to the script itself, please edit <code>src/salvattore.js</code> and send us a pull request.</p>