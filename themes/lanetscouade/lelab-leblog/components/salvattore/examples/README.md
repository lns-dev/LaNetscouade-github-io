<h1>How to run</h1>
<p>Run a HTTP server in the root folder, where <code>salvattore.js</code> or <code>.min.js</code> are and then go to <a href="http://localhost:8000/examples/timeline.html">http://localhost:8000/examples/timeline.html</a></p>
<p>The easiest way to run a HTTP server is by using the <code>python -m SimpleHTTPServer</code> command.</p>