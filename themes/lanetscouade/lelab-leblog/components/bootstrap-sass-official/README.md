<h1>Bootstrap for Sass <a href="http://travis-ci.org/twbs/bootstrap-sass"><img src="http://img.shields.io/travis/twbs/bootstrap-sass.svg" alt="Build Status" /></a></h1>
<p><code>bootstrap-sass</code> is a Sass-powered version of <a href="http://github.com/twbs/bootstrap">Bootstrap</a>, ready to drop right into your Sass powered applications.</p>
<h2>Installation</h2>
<p>Please see the appropriate guide for your environment of choice:</p>
<h3>a. Ruby on Rails</h3>
<p><code>bootstrap-sass</code> is easy to drop into Rails with the asset pipeline.</p>
<p>In your Gemfile you need to add the <code>bootstrap-sass</code> gem, and ensure that the <code>sass-rails</code> gem is present - it is added to new Rails applications by default.</p>
<pre><code class="language-ruby">gem 'sass-rails', '&gt;= 3.2'
gem 'bootstrap-sass', '~&gt; 3.1.1'</code></pre>
<p><code>bundle install</code> and restart your server to make the files available through the pipeline.</p>
<h4>Rails 3.2.x</h4>
<p>Rails 3.2 is <a href="http://guides.rubyonrails.org/maintenance_policy.html">no longer maintained for bugfixes</a>, and you should upgrade as soon as possible.</p>
<p>If you must use it, make sure bootstrap-sass is moved out of the <code>:assets</code> group.
This is because, by default in Rails 3.2, assets group gems are not required in <code>production</code>.
However, for pre-compilation to succeed in production, <code>bootstrap-sass</code> gem must be required.</p>
<p>Starting with bootstrap-sass v3.1.1.1, due to the structural changes from upstream you will need these
backported asset pipeline gems on Rails 3.2. There is more on why this is necessary in
<a href="https://github.com/twbs/bootstrap-sass/issues/523">https://github.com/twbs/bootstrap-sass/issues/523</a> and <a href="https://github.com/twbs/bootstrap-sass/issues/578">https://github.com/twbs/bootstrap-sass/issues/578</a>.</p>
<pre><code class="language-ruby">gem 'sprockets-rails', '=2.0.0.backport1'
gem 'sprockets', '=2.2.2.backport2'
gem 'sass-rails', github: 'guilleiguaran/sass-rails', branch: 'backport'</code></pre>
<h3>b. Compass without Rails</h3>
<p>Install the gem</p>
<pre><code class="language-sh">gem install bootstrap-sass</code></pre>
<p>If you have an existing Compass project:</p>
<pre><code class="language-ruby"># config.rb:
require 'bootstrap-sass'</code></pre>
<pre><code class="language-console">bundle exec compass install bootstrap</code></pre>
<p>If you are creating a new Compass project, you can generate it with bootstrap-sass support:</p>
<pre><code class="language-console">bundle exec compass create my-new-project -r bootstrap-sass --using bootstrap</code></pre>
<p>or, alternatively, if you're not using a Gemfile for your dependencies:</p>
<pre><code class="language-console">compass create my-new-project -r bootstrap-sass --using bootstrap</code></pre>
<p>This will create a new Compass project with the following files in it:</p>
<ul>
<li><a href="/templates/project/_variables.sass.erb">_variables.scss</a> - all of bootstrap variables (override them here).</li>
<li><a href="/templates/project/styles.sass">styles.scss</a> - main project SCSS file, import <code>variables</code> and <code>bootstrap</code>.</li>
</ul>
<p>Some bootstrap-sass mixins may conflict with the Compass ones.
If this happens, change the import order so that Compass mixins are loaded later.</p>
<h3>c. Ruby without Compass / Rails</h3>
<p>Require the gem, and load paths and Sass helpers will be configured automatically:</p>
<pre><code class="language-ruby">require 'bootstrap-sass'</code></pre>
<h3>d. Node.js / Bower</h3>
<p>Using bootstrap-sass as a Bower package is still being tested. It is compatible with node-sass 0.8.3+. You can install it with:</p>
<pre><code class="language-bash">bower install bootstrap-sass-official</code></pre>
<p><code>bootstrap-sass</code> is taken so make sure you use the command above.</p>
<p>Sass, JS, and all other assets are located at <a href="/vendor/assets">vendor/assets</a>.</p>
<p>By default, <code>bower.json</code> main field list only the main <code>bootstrap.scss</code> and all the static assets (fonts and JS).
This is compatible by default with asset managers such as <a href="https://github.com/taptapship/wiredep">wiredep</a>.</p>
<h4>Mincer</h4>
<p>If you use <a href="https://github.com/nodeca/mincer">mincer</a> with node-sass, import bootstrap into a <code>.css.ejs.scss</code> file  like so:</p>
<pre><code class="language-scss">// Import mincer asset paths helper integration
@import "bootstrap-mincer";
@import "bootstrap";</code></pre>
<p>See also this <a href="/test/dummy_node_mincer/manifest.js">example manifest.js</a> for mincer.</p>
<h4>Number precision</h4>
<p>bootstrap-sass <a href="https://github.com/twbs/bootstrap-sass/issues/409">requires</a> minimum <a href="http://sass-lang.com/documentation/Sass/Script/Number.html#precision-class_method">Sass number precision</a> of 10 (default is 5).</p>
<p>When using ruby Sass compiler with the bower version you can enforce the limit with:</p>
<pre><code class="language-ruby">::Sass::Script::Number.precision = [10, ::Sass::Script::Number.precision].max</code></pre>
<p>Precision option is now available in libsass, but it has not made into node-sass yet.</p>
<h4>JS and fonts</h4>
<p>Assets are discovered automatically on Rails, Sprockets, Compass, and Node + Mincer, using native asset path helpers.</p>
<p>Otherwise the fonts are referenced as:</p>
<pre><code class="language-sass">"#{$icon-font-path}#{$icon-font-name}.eot"</code></pre>
<p><code>$icon-font-path</code> defaults to <code>bootstrap/</code>.</p>
<p>When not using an asset pipeline, you can copy fonts and JS from bootstrap-sass, they are located at <a href="/vendor/assets">vendor/assets</a>:</p>
<pre><code class="language-bash">mkdir public/fonts
cp -r $(bundle show bootstrap-sass)/vendor/assets/fonts/ public/fonts/
mkdir public/javascripts
cp -r $(bundle show bootstrap-sass)/vendor/assets/javascripts/ public/javascripts/</code></pre>
<h2>Usage</h2>
<h3>Sass</h3>
<p>Import Bootstrap into a Sass file (for example, <code>application.css.scss</code>) to get all of Bootstrap's styles, mixins and variables!
We recommend against using <code>//= require</code> directives, since none of your other stylesheets will be <a href="https://github.com/twbs/bootstrap-sass/issues/79#issuecomment-4428595">able to access</a> the Bootstrap mixins or variables.</p>
<pre><code class="language-scss">@import "bootstrap";</code></pre>
<p>You can also include optional bootstrap theme:</p>
<pre><code class="language-scss">@import "bootstrap/theme";</code></pre>
<p>The full list of bootstrap variables can be found <a href="http://getbootstrap.com/customize/#less-variables">here</a>. You can override these by simply redefining the variable before the <code>@import</code> directive, e.g.:</p>
<pre><code class="language-scss">$navbar-default-bg: #312312;
$light-orange: #ff8c00;
$navbar-default-color: $light-orange;

@import "bootstrap";</code></pre>
<p>You can also import components explicitly. To start with a full list of modules copy this file from the gem:</p>
<pre><code class="language-bash">cp $(bundle show bootstrap-sass)/vendor/assets/stylesheets/bootstrap.scss \
 app/assets/stylesheets/bootstrap-custom.scss</code></pre>
<p>Comment out components you do not want from <code>bootstrap-custom</code>.</p>
<p>In <code>application.sass</code>, replace <code>@import 'bootstrap'</code> with:</p>
<pre><code class="language-scss">  @import 'bootstrap-custom';</code></pre>
<h3>Javascript</h3>
<p>We have a helper that includes all Bootstrap javascripts. If you use Rails (or Sprockets separately),
put this in your Javascript manifest (usually in <code>application.js</code>) to load the files in the <a href="/vendor/assets/javascripts/bootstrap.js">correct order</a>:</p>
<pre><code class="language-js">// Loads all Bootstrap javascripts
//= require bootstrap</code></pre>
<p>You can also load individual modules, provided you also require any dependencies. You can check dependencies in the <a href="http://getbootstrap.com/javascript/#transitions">Bootstrap JS documentation</a>.</p>
<pre><code class="language-js">//= require bootstrap/scrollspy
//= require bootstrap/modal
//= require bootstrap/dropdown</code></pre>
<hr />
<h2>Development and Contributing</h2>
<p>If you'd like to help with the development of bootstrap-sass itself, read this section.</p>
<h3>Upstream Converter</h3>
<p>Keeping bootstrap-sass in sync with upstream changes from Bootstrap used to be an error prone and time consuming manual process. With Bootstrap 3 we have introduced a converter that automates this.</p>
<p><strong>Note: if you're just looking to <em>use</em> Bootstrap 3, see the <a href="#installation">installation</a> section above.</strong></p>
<p>Upstream changes to the Bootstrap project can now be pulled in using the <code>convert</code> rake task.</p>
<p>Here's an example run that would pull down the master branch from the main <a href="https://github.com/twbs/bootstrap">twbs/bootstrap</a> repo:</p>
<pre><code>rake convert</code></pre>
<p>This will convert the latest LESS to Sass and update to the latest JS.
To convert a specific branch or version, pass the branch name or the commit hash as the first task argument:</p>
<pre><code>rake convert[e8a1df5f060bf7e6631554648e0abde150aedbe4]</code></pre>
<p>The latest converter script is located <a href="https://github.com/twbs/bootstrap-sass/blob/master/tasks/converter/less_conversion.rb">here</a> and does the following:</p>
<ul>
<li>Converts upstream bootstrap LESS files to its matching SCSS file.</li>
<li>Copies all upstream JavaScript into <code>vendor/assets/javascripts/bootstrap</code></li>
<li>Generates a javascript manifest at <code>vendor/assets/javascripts/bootstrap.js</code></li>
<li>Copies all upstream font files into <code>vendor/assets/fonts/bootstrap</code></li>
<li>Sets <code>Bootstrap::BOOTSTRAP_SHA</code> in <a href="https://github.com/twbs/bootstrap-sass/blob/master/lib/bootstrap-sass/version.rb">version.rb</a> to the branch sha.</li>
</ul>
<p>This converter fully converts original LESS to SCSS. Conversion is automatic but requires instructions for certain transformations (see converter output).
Please submit GitHub issues tagged with <code>conversion</code>.</p>
<h2>Credits</h2>
<p>bootstrap-sass has a number of major contributors:</p>
<!-- feel free to make these link wherever you wish -->
<ul>
<li><a href="https://twitter.com/thomasmcdonald_">Thomas McDonald</a></li>
<li><a href="http://www.trisweb.com">Tristan Harward</a></li>
<li>Peter Gumeson</li>
<li><a href="https://github.com/glebm">Gleb Mazovetskiy</a></li>
</ul>
<p>and a <a href="https://github.com/twbs/bootstrap-sass/graphs/contributors">significant number of other contributors</a>.</p>
<h2>You're in good company</h2>
<p>bootstrap-sass is used to build some awesome projects all over the web, including
<a href="http://diasporaproject.org/">Diaspora</a>, <a href="https://github.com/sferik/rails_admin">rails_admin</a>,
Michael Hartl's <a href="http://railstutorial.org/">Rails Tutorial</a>, <a href="http://gitlabhq.com/">gitlabhq</a> and
<a href="http://kandanapp.com/">kandan</a>.</p>